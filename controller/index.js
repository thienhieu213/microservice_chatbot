var express = require('express');
var cookieParser = require('cookie-parser');
var favicon = require('express-favicon');
var https = require('https');
var fs = require('fs');

var options = {
  key: fs.readFileSync('server.key.pem'),
  cert: fs.readFileSync('server.crt.pem')
};

var app = express();
app.use(cookieParser());
app.use(favicon(__dirname + '/public/img/favicon.png'));
app.use(express.static("public")); 
app.set("view engine", "ejs");
app.set("views","./views");

//var server = require("http").Server(app);
var server = https.createServer(options, app).listen(443);
server.listen(process.env.PORT || 80);
var io = require('socket.io').listen(server);
var io_client = require('socket.io-client');
var ip_address = 'http://localhost';

/******************************************** Area Connect Socket **********************************************/
// socket_bookingroom.on('connect', function () {
//   // socket connected
//   socket_bookingroom.emit('controller', "hellu");
//   socket_bookingroom.on('micro', function (data) {
//     console.log('new message from micro:', data);
//   });
// });
var betaversion_open = false;
var bookingflight_open = false;
var bookingroom_open = false;
var goimpact_open = false;
var mcdonald_open = false;
var bookingroom_ver_2_open = false;

io.on("connection", function(socket){
  //Mc Donald's
  if (mcdonald_open){
    var socket_mcdonald = io_client.connect(ip_address + ':9000');
    socket.on("mcdonald_chatbot",function(data){
      socket_mcdonald.emit('chatbot', data);
    })
    socket_mcdonald.on('chatbot',function(data){
      socket.emit('mcdonald_chatbot',data);
    });
  
    socket.on("mcdonald_setlanguage",function(data){
      socket_mcdonald.emit('setlanguage', data);
    })
    socket_mcdonald.on('setlanguage',function(data){
      socket.emit('mcdonald_setlanguage',data);
    });
  
    socket.on("mcdonald_init_data",function(data){
      socket_mcdonald.emit('init_data', data);
    })
    socket_mcdonald.on('init_data',function(data){
      socket.emit('mcdonald_init_data',data);
    });
  
    socket.on("mcdonald_intent",function(data){
      socket_mcdonald.emit('intent', data);
    })
    socket_mcdonald.on('intent',function(data){
      socket.emit('mcdonald_intent',data);
    });
  }

  ///////////////////////////////////////////

  //Goimpact chatbot
  if (goimpact_open){
    var socket_goimpact = io_client.connect(ip_address + ':9001',{secure: true});
    socket.on("goimpact_chatbot",function(data){
      socket_goimpact.emit('chatbot', data);
    })
    socket_goimpact.on('chatbot',function(data){
      socket.emit('goimpact_chatbot',data);
    });
  
    socket.on("goimpact_init_data",function(data){
      socket_goimpact.emit('init_data', data);
    })
    socket_goimpact.on('init_data',function(data){
      socket.emit('goimpact_init_data',data);
    });
  }
  
  ///////////////////////////////////////////

  //Booking Room chatbot
  if (bookingroom_open){
    var socket_bookingroom = io_client.connect(ip_address + ':9002');
    socket.on("bookingroom_chatbot",function(data){
      socket_bookingroom.emit('chatbot', data);
    })
    socket_bookingroom.on('chatbot',function(data){
      socket.emit('bookingroom_chatbot',data);
    });
  
    socket.on("bookingroom_botMess_2FA",function(data){
      socket_bookingroom.emit('botMess_2FA', data);
    })
    socket_bookingroom.on('botMess_2FA',function(data){
      socket.emit('bookingroom_botMess_2FA',data);
    });
  
    socket.on("bookingroom_clientMess_2FA",function(data){
      socket_bookingroom.emit('clientMess_2FA', data);
    })
    socket_bookingroom.on('clientMess_2FA',function(data){
      socket.emit('bookingroom_clientMess_2FA',data);
    });
  
    socket.on("bookingroom_setPhoneNumber",function(data){
      socket_bookingroom.emit('setPhoneNumber', data);
    })
    socket_bookingroom.on('setPhoneNumber',function(data){
      socket.emit('bookingroom_setPhoneNumber',data);
    });
  
    socket.on("bookingroom_init_data",function(data){
      socket_bookingroom.emit('init_data', data);
    })
    socket_bookingroom.on('init_data',function(data){
      socket.emit('bookingroom_init_data',data);
    });
  }
  
  ///////////////////////

  //Booking Flight chatbot
  if (bookingflight_open){
    var socket_bookingflight = io_client.connect(ip_address + ':9003');
    socket.on("bookingflight_chatbot",function(data){
      socket_bookingflight.emit('chatbot', data);
    })
    socket_bookingflight.on('chatbot',function(data){
      socket.emit('bookingflight_chatbot',data);
    });
    socket.on("bookingflight_botMess_2FA",function(data){
      socket_bookingflight.emit('botMess_2FA', data);
    })
    socket_bookingflight.on('botMess_2FA',function(data){
      socket.emit('bookingflight_botMess_2FA',data);
    });
  
    socket.on("bookingflight_clientMess_2FA",function(data){
      socket_bookingflight.emit('clientMess_2FA', data);
    })
    socket_bookingflight.on('clientMess_2FA',function(data){
      socket.emit('bookingflight_clientMess_2FA',data);
    });
  
    socket.on("bookingflight_setPhoneNumber",function(data){
      socket_bookingflight.emit('setPhoneNumber', data);
    })
    socket_bookingflight.on('setPhoneNumber',function(data){
      socket.emit('bookingflight_setPhoneNumber',data);
    });
  
    socket.on("bookingflight_init_data",function(data){
      socket_bookingflight.emit('init_data', data);
    })
    socket_bookingflight.on('init_data',function(data){
      socket.emit('bookingflight_init_data',data);
    });
  }
  
  ////////////////////////

  //Beta Version Noiz
  if (betaversion_open){
    var socket_betaversion = io_client.connect(ip_address + ':9004');
    socket.on("betaversion_chatbot",function(data){
      socket_betaversion.emit('chatbot', data);
    })
    socket_betaversion.on('chatbot',function(data){
      socket.emit('betaversion_chatbot',data);
    });
  
    socket.on("betaversion_intent",function(data){
      socket_betaversion.emit('intent', data);
    })
    socket_betaversion.on('intent',function(data){
      socket.emit('betaversion_intent',data);
    });
  }
  ///////////////////////////////////////////

  //Booking Room ver 2 chatbot
  if (bookingroom_ver_2_open){
    var socket_bookingroom_ver_2 = io_client.connect(ip_address + ':9005');
    socket.on("bookingroom_ver_2_chatbot",function(data){
      socket_bookingroom_ver_2.emit('chatbot', data);
    })
    socket_bookingroom_ver_2.on('chatbot',function(data){
      socket.emit('bookingroom_ver_2_chatbot',data);
    });
  
    socket.on("bookingroom_ver_2_botMess_2FA",function(data){
      socket_bookingroom_ver_2.emit('botMess_2FA', data);
    })
    socket_bookingroom_ver_2.on('botMess_2FA',function(data){
      socket.emit('bookingroom_ver_2_botMess_2FA',data);
    });
  
    socket.on("bookingroom_ver_2_clientMess_2FA",function(data){
      socket_bookingroom_ver_2.emit('clientMess_2FA', data);
    })
    socket_bookingroom_ver_2.on('clientMess_2FA',function(data){
      socket.emit('bookingroom_ver_2_clientMess_2FA',data);
    });
  
    socket.on("bookingroom_ver_2_setPhoneNumber",function(data){
      socket_bookingroom_ver_2.emit('setPhoneNumber', data);
    })
    socket_bookingroom_ver_2.on('setPhoneNumber',function(data){
      socket.emit('bookingroom_ver_2_setPhoneNumber',data);
    });
  
    socket.on("bookingroom_ver_2_init_data",function(data){
      socket_bookingroom_ver_2.emit('init_data', data);
    })
    socket_bookingroom_ver_2.on('init_data',function(data){
      socket.emit('bookingroom_ver_2_init_data',data);
    });
  }
  
  ///////////////////////
})
/************************************************************************************************************/

/******************************************** Area Render for client **********************************************/
app.get("/", function(req,res){
  if (!req.cookies.NOIZ_APP_ID){ //don't have cookie
    var NOIZ_APP_ID = getRandomInt(1000000000,100000000);
    res.cookie('NOIZ_APP_ID',NOIZ_APP_ID, { maxAge: 900000, httpOnly: false });
  }
  res.render("index");
});
app.get("/bookingroom", function(req,res){
  turnOffConnect();
  bookingroom_open = true;
  if (!req.cookies.NOIZ_APP_ID){ //don't have cookie
    var NOIZ_APP_ID = getRandomInt(1000000000,100000000);
    res.cookie('NOIZ_APP_ID',NOIZ_APP_ID, { maxAge: 900000, httpOnly: false });
  }
  res.render("bookingroom");
});
app.get("/bookingroom_size", function(req,res){
  turnOffConnect();
  bookingroom_open = true;
  if (!req.cookies.NOIZ_APP_ID){ //don't have cookie
    var NOIZ_APP_ID = getRandomInt(1000000000,100000000);
    res.cookie('NOIZ_APP_ID',NOIZ_APP_ID, { maxAge: 900000, httpOnly: false });
  }
  var width = req.query.width;
  var height = req.query.height;
  res.render("bookingroom_size",{width,height});
})
// app.get("/mvp-demo-1", function(req,res){
//   turnOffConnect();
//   bookingroom_ver_2_open = true;
//   if (!req.cookies.NOIZ_APP_ID){ //don't have cookie
//     var NOIZ_APP_ID = getRandomInt(1000000000,100000000);
//     res.cookie('NOIZ_APP_ID',NOIZ_APP_ID, { maxAge: 900000, httpOnly: false });
//   }
//   res.render("bookingroom_new");
// });
app.get("/mvp-demo-1", function(req,res){
  turnOffConnect();
  bookingroom_ver_2_open = true;
  if (!req.cookies.NOIZ_APP_ID){ //don't have cookie
    var NOIZ_APP_ID = getRandomInt(1000000000,100000000);
    res.cookie('NOIZ_APP_ID',NOIZ_APP_ID, { maxAge: 900000, httpOnly: false });
  }
  var width = req.query.width;
  var height = req.query.height;
  res.render("bookingroom_new_size",{width,height});
})
app.get("/bookingflight", function(req,res){
  turnOffConnect();
  bookingflight_open = true;
  if (!req.cookies.NOIZ_APP_ID){ //don't have cookie
    var NOIZ_APP_ID = getRandomInt(1000000000,100000000);
    res.cookie('NOIZ_APP_ID',NOIZ_APP_ID, { maxAge: 900000, httpOnly: false });
  }
  res.render("bookingflight");
});
app.get("/bookingflight_size", function(req,res){
  turnOffConnect();
  bookingflight_open = true;
  if (!req.cookies.NOIZ_APP_ID){ //don't have cookie
    var NOIZ_APP_ID = getRandomInt(1000000000,100000000);
    res.cookie('NOIZ_APP_ID',NOIZ_APP_ID, { maxAge: 900000, httpOnly: false });
  }
  var width = req.query.width;
  var height = req.query.height;
  res.render("bookingflight_size",{width,height});
})
app.get("/goimpact", function(req,res){
  turnOffConnect();
  goimpact_open = true;
  if (!req.cookies.GOIMPACT_ID){ //don't have cookie
    var GOIMPACT_ID = getRandomInt(1000000000,100000000);
    res.cookie('GOIMPACT_ID',GOIMPACT_ID, { maxAge: 900000, httpOnly: false });
  }
  res.render("goimpact");
});
app.get("/betaversion", function(req,res){
  turnOffConnect();
  betaversion_open = true;
  if (!req.cookies.NOIZ_APP_ID){ //don't have cookie
    var NOIZ_APP_ID = getRandomInt(1000000000,100000000);
    res.cookie('NOIZ_APP_ID',NOIZ_APP_ID, { maxAge: 900000, httpOnly: false });
  }
  res.render("betaVersionNoiz");
});
app.get("/mcdonald", function(req,res){
  //turnOffConnect();
  mcdonald_open = true;
  if (!req.cookies.session_id){
    var sessionId = getRandomInt(1000000000,100000000);
    res.cookie('session_id',sessionId, { maxAge: 900000, httpOnly: false });
  }
  res.render("mcdonald");
});
app.get("/mcdonald_size", function(req,res){
  turnOffConnect();
  mcdonald_open = true;
  if (!req.cookies.session_id){
    var sessionId = getRandomInt(1000000000,100000000);
    res.cookie('session_id',sessionId, { maxAge: 900000, httpOnly: false });
  }
  var width = req.query.width;
  var height = req.query.height;
  res.render("mcdonald_size",{width,height});
})
/************************************************************************************************************/

/******************************************** Area Function **********************************************/
function getRandomInt(max,min) {
  return Math.floor((Math.random() * (max - min)) + min);
}
function turnOffConnect(){
  betaversion_open = false;
  bookingflight_open = false;
  bookingroom_open = false;
  goimpact_open = false;
  mcdonald_open = false;
}
/************************************************************************************************************/
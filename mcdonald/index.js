process.env.GOOGLE_APPLICATION_CREDENTIALS = 'file.txt'
var express = require('express');
var MongoClient = require('mongodb').MongoClient;

var app = express();
app.set("view engine", "ejs");
app.set("views","./views");
var server = require("http").Server(app);
var io = require('socket.io')(server);
server.listen(process.env.PORT || 9000);

// You can find your project ID in your Dialogflow agent settings
const projectId = 'chatbot-mcdonald-s-dev';
 
// Instantiate a DialogFlow client.
const dialogflow = require('dialogflow');
const sessionClient = new dialogflow.SessionsClient();

// Define session path
// const sessionPath = sessionClient.sessionPath(projectId, sessionId);

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'support@fdssoft.com',
    pass: '123alabama'
  }
});

io.on("connection", function(socket){
  // var sesId;
  // var sessionPath = "1";
  // socket.on("socket_id",function(data){
  //   sesId = data;
  //   console.log(sesId);
  //   sessionPath = sessionClient.sessionPath(projectId, sesId);
  // })
  const sesId = socket.id;
  sessionPath = sessionClient.sessionPath(projectId, sesId);
  var language = "en-US"; 
  socket.on("setlanguage",function(data){
    language = data;
  })
  socket.on("init_data",function(data){
    initMess(data,sesId);
  })
  socket.on("chatbot",function(data){
    clientMess(data,sesId);
    
    if (validateEmail(data)){
      var mailOptions = {
        from: 'support@fdssoft.com',
        to: data,
        subject: 'Test Server Email',
        text: 'This is code to discount $20: ABCD1234'
      };
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          // console.log('Email sent: ' + info.response);
        }
      });
    }
    
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: data,
                languageCode: language,//en-US
            },
        },
    };
    console.log(request.session);
    sessionClient.detectIntent(request).then(responses => {
      const result = responses[0].queryResult;
      // console.log(result.intent);
      socket.emit("intent",result.intent.displayName);
      if (result.fulfillmentText != ""){
        var data_type_2 = {
          'response':{
            'listValue':{
              'values':[
                {
                  'stringValue': result.fulfillmentText,
                  'kind': 'stringValue'
                }
                
              ]
            },
            'kind': 'listValue'
          },
          'answer':{
            'listValue':{
              'values':[],
            },
            'kind': 'listValue'
          }
        }
        socket.emit("chatbot",data_type_2);
        botMess(data_type_2,sesId);
      }
      else{
        socket.emit("chatbot",result.fulfillmentMessages[0].payload.fields);
        botMess(result.fulfillmentMessages[0].payload.fields,sesId);
      }      
    })
    .catch(err => {
      console.error('ERRORerr: ' + err);
    });
  })
})

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
function connectDatabase(){
  var dbuser = 'dialogflow-test-1';
  var dbpassword = 'a123456789';
  var database = 'dialogflow';

  var url = 'mongodb://' + dbuser + ":" + dbpassword + "@ds253587.mlab.com:53587/" + database;

  return url;
}

function initMess(data,sessionId){
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    } 
    db.collection('mcdonalds').findOne(
      {
        "session_id": sessionId
      },
      function(err, object) {
        if (err){
          console.log(err);
          console.log("error");
        }
        else{
          if (object){
          }
          else{
            db.collection('mcdonalds').insert(data,function(err,record){
            })
          }
        }
      }
    )
  })
}
function botMess(data,sessionId){
  var time = new Date().getTime();
  var timestamp = new Object();
  timestamp[time] =  {
    'who' : "bot",
    'response' : data.response.listValue.values,
    'answer' : data.answer.listValue.values
  };
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    }
    db.collection('mcdonalds').findAndModify(
      {
        "session_id": sessionId
      }, // query
      [['_id','asc']],  // sort order
      {
        $push:{
          timestamp:timestamp
        }
      }, // replacement
      {}, // options
      function(err, object) {
          if (err){
              console.warn(err.message); 
              console.warn("error"); 
          }
          else{
              if (object){
              }
              else{
              }
          }
      }
    )
  })
}

function clientMess(data,sessionId){
  var time = new Date().getTime();
  var timestamp = new Object();
  timestamp[time] =  {
    'who' : "client",
    'response' : data
  };
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    }
    db.collection('mcdonalds').findAndModify(
      {
        "session_id": sessionId
      }, // query
      [['_id','asc']],  // sort order
      {
        $push:{
          timestamp:timestamp
        }
      }, // replacement
      {}, // options
      function(err, object) {
        if (err){
            console.warn(err.message);  // returns error if no matching object found
            console.warn("error"); 
        }
        else{
          if (object){
          }
          else{
          }
        }
      }
    )
  })
}

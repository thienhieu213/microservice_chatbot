process.env.GOOGLE_APPLICATION_CREDENTIALS = 'file.txt'
var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var https = require('https');
var fs = require('fs');

var app = express();
app.set("view engine", "ejs");
app.set("views","./views");

var server = require("http").Server(app);
var io = require('socket.io')(server);

// var options = {
//   key: fs.readFileSync('server.key.pem'),
//   cert: fs.readFileSync('server.crt.pem')
// };
// var server = https.createServer(options, app);
// var io = require('socket.io').listen(server);

server.listen(process.env.PORT || 9001);

// You can find your project ID in your Dialogflow agent settings
const projectId = 'noizchain-799be';
// const sessionId = 'quickstart-session-id';
 
// Instantiate a DialogFlow client.
const dialogflow = require('dialogflow');
const sessionClient = new dialogflow.SessionsClient();

// Define session path


io.on("connection", function(socket){
  const sesId = socket.id;
  const sessionPath = sessionClient.sessionPath(projectId, sesId);
  var language = "en-US"; 

  socket.on("init_data",function(data){
    console.log(data);
    data.session_id = sesId;
    initMess(data,sesId);
  })

  socket.on("chatbot",function(data){
    clientMess(data,sesId);
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: data,
                languageCode: language,//en-US
            },
        },
    };

  sessionClient
    .detectIntent(request)
    .then(responses => {
      const result = responses[0].queryResult;
      if (result.fulfillmentMessages[0].text){ //webhook response
        socket.emit("chatbot",result.fulfillmentMessages[0].text.text[0]);
        var data_type = JSON.parse(result.fulfillmentMessages[0].text.text[0]);
        botMess(data_type,sesId);
      }
    })
    .catch(err => {
      console.error('ERRORerr');
    });
  })
})

function connectDatabase(){
  var dbuser = 'noizchain_connor_1';
  var dbpassword = 'a123456789';
  var database = 'noizchain_connor_project_1';

  var url = 'mongodb://' + dbuser + ":" + dbpassword + "@ds121871.mlab.com:21871/" + database;

  return url;
}

function initMess(data,sessionId){
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    } 
    db.collection('goimpact').findOne(
      {
        "session_id": sessionId
      },
      function(err, object) {
        if (err){
          console.log(err);
          console.log("error");
        }
        else{
          if (object){
          }
          else{
            db.collection('goimpact').insert(data,function(err,record){
            })
          }
        }
      }
    )
  })
}
function botMess(data,sessionId){
  var time = getTime();
  var timestamp = new Object();
  timestamp[time] =  {
    'who' : "bot",
    'response' : data.response,
    'answer' : data.answer,
    'image': data.image
  };
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    }
    db.collection('goimpact').findAndModify(
      {
        "session_id": sessionId
      }, // query
      [['_id','asc']],  // sort order
      {
        $push:{
          timestamp:timestamp
        }
      }, // replacement
      {}, // options
      function(err, object) {
          if (err){
              console.warn(err.message); 
              console.warn("error"); 
          }
          else{
              if (object){
              }
              else{
              }
          }
      }
    )
  })
}

function clientMess(data,sessionId){
  var time = getTime();
  var timestamp = new Object();
  timestamp[time] =  {
    'who' : "client",
    'response' : data
  };
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    }
    db.collection('goimpact').findAndModify(
      {
        "session_id": sessionId
      }, // query
      [['_id','asc']],  // sort order
      {
        $push:{
          timestamp:timestamp
        }
      }, // replacement
      {}, // options
      function(err, object) {
        if (err){
            console.warn(err.message);  // returns error if no matching object found
            console.warn("error"); 
        }
        else{
          if (object){
          }
          else{
          }
        }
      }
    )
  })
}
function getTime(){
  var d = new Date();
  var day = d.getDate();
  var month = d.getMonth() + 1;
  var year = d.getFullYear();
  var hour = d.getHours();
  var minute = d.getMinutes();
  var second = d.getSeconds();

  var time = hour + ":" + minute + ":" + second + " | " + month + "/" + day + "/" + year;
  return time;
}


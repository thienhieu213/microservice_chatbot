process.env.GOOGLE_APPLICATION_CREDENTIALS = 'file.txt'
var express = require('express');
var MongoClient = require('mongodb').MongoClient;

var app = express();
app.set("view engine", "ejs");
app.set("views","./views");
var server = require("http").Server(app);
var io = require('socket.io')(server);
server.listen(process.env.PORT || 9004);

// You can find your project ID in your Dialogflow agent settings
const projectId = 'noizchain-connor';
 
// Instantiate a DialogFlow client.
const dialogflow = require('dialogflow');
const sessionClient = new dialogflow.SessionsClient();

// Define session path

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'support@fdssoft.com',
    pass: '123alabama'
  }
});

var customer_email = "";
var customer_name = "";
var customer_age = "";
var customer_country = "";
var customer_gender = "";
io.on("connection", function(socket){
  const sessionId = socket.id;
  const sessionPath = sessionClient.sessionPath(projectId, sessionId);
  var language = "en-US"; 
  socket.on("setlanguage",function(data){
    language = data;
  })
  socket.on("chatbot",function(data){
    
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: data,
                languageCode: language,//en-US
            },
        },
    };

    sessionClient.detectIntent(request).then(responses => {     
      const result = responses[0].queryResult;
      if (result.parameters.fields.customer_email){
        customer_email = result.parameters.fields.customer_email.stringValue;
      }
      if (result.parameters.fields.customer_name){
        if (result.parameters.fields.customer_name.structValue){
          if (result.parameters.fields.customer_name.structValue.fields["more-name"]){
            customer_name = result.parameters.fields.customer_name.structValue.fields["more-name"].stringValue;
          }
          else if (result.parameters.fields.customer_name.structValue.fields["given-name"]){
            customer_name = result.parameters.fields.customer_name.structValue.fields["given-name"].stringValue;
          }
          else if (result.parameters.fields.customer_name.structValue.fields["last-name"]){
            customer_name = result.parameters.fields.customer_name.structValue.fields["last-name"].stringValue;
          }
        }
      }
      if (result.parameters.fields.customer_age){
        customer_age = result.parameters.fields.customer_age.structValue.fields.amount.numberValue;
      }
      if (result.parameters.fields.customer_country){
        customer_country = result.parameters.fields.customer_country.stringValue;
      }
      if (result.parameters.fields.customer_gender){
        customer_gender = result.parameters.fields.customer_gender.stringValue;
      }
      socket.emit("intent",result.intent.displayName); // noti intent for client-side

      if (result.intent.displayName == "NoizChain_Connor_AskWhereToLive-no" || result.intent.displayName == "NoizChain_Connor_AskWhereToLive-yes"){

        sendEmail(customer_email);
        var data = {
          "email": customer_email,
          "age": customer_age,
          "country": customer_country,
          "gender": customer_gender,
          "name": customer_name,
        }
        storageData(data);
      }


      if (result.fulfillmentText != ""){ //normal response and webhook response
        // console.log("1");
        var data_type = configData(result.fulfillmentText);
        socket.emit("chatbot",data_type);
        // botMess(data_type,sessionId);
      }
      else if (result.fulfillmentMessages[0].payload){ //custom response
        // console.log("2");
        socket.emit("chatbot",result.fulfillmentMessages[0].payload.fields);
        // botMess(result.fulfillmentMessages[0].payload.fields,sessionId);
      } 
      else if (result.fulfillmentMessages[0].text){ //question's parameter response
        // console.log("3");
        var data_type = configData(result.fulfillmentMessages[0].text.text[0]);
        socket.emit("chatbot",data_type);
        // botMess(data_type,sessionId);
      }

    })
    .catch(err => {
      console.error('ERRORerr: ' + err);
    });
  })
})


function sendEmail(email){
  if (validateEmail(email)){
    var mailOptions = {
      from: 'support@fdssoft.com',
      to: email,
      subject: 'NoizChain',
      text: 'Welcome to NOIZ Beta Testers Community'
    };
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        // console.log('Email sent: ' + info.response);
      }
    });
  }
}
function configData(data){
  var data_type = {
    'response':{
      'listValue':{
        'values':[
          {
            'stringValue': data,
            'kind': 'stringValue'
          }
        ]
      },
      'kind': 'listValue'
    },
    'answer':{
      'listValue':{
        'values':[],
      },
      'kind': 'listValue'
    }
  }
  return data_type;
}
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
function getRandomInt(max,min) {
  return Math.floor((Math.random() * (max - min)) + min);
}
function connectDatabase(){
  var dbuser = 'noizchain_connor_1';
  var dbpassword = 'a123456789';
  var database = 'noizchain_connor_project_1';

  var url = 'mongodb://' + dbuser + ":" + dbpassword + "@ds121871.mlab.com:21871/" + database;

  return url;
}

function initMess(data,sessionId){
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    } 
    db.collection('mcdonalds').findOne(
      {
        "session_id": sessionId
      },
      function(err, object) {
        if (err){
          console.log(err);
          console.log("error");
        }
        else{
          if (object){
            
            db.collection('mcdonalds').update(
              {"session_id":sessionId},
              {
                $set:{
                  "name":data.name,
                  "phone_number":data.phone_number,
                  "food":data.food,
                  "color":data.color
                }
              }
            )
            console.log("update");
          }
          else{
            db.collection('mcdonalds').insert(data,function(err,record){
            })
            console.log("insert");
          }
        }
      }
    )
  })
}
function botMess(data,sessionId){
  var time = new Date().getTime();
  var timestamp = new Object();
  timestamp[time] =  {
    'who' : "bot",
    'response' : data.response.listValue.values,
    'answer' : data.answer.listValue.values
  };
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    }
    db.collection('mcdonalds').findAndModify(
      {
        "session_id": sessionId
      }, // query
      [['_id','asc']],  // sort order
      {
        $push:{
          timestamp:timestamp
        }
      }, // replacement
      {}, // options
      function(err, object) {
          if (err){
              console.warn(err.message); 
              console.warn("error"); 
          }
          else{
              if (object){
              }
              else{
              }
          }
      }
    )
  })
}

function clientMess(data,sessionId){
  var time = new Date().getTime();
  var timestamp = new Object();
  timestamp[time] =  {
    'who' : "client",
    'response' : data
  };
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    }
    db.collection('mcdonalds').findAndModify(
      {
        "session_id": sessionId
      }, // query
      [['_id','asc']],  // sort order
      {
        $push:{
          timestamp:timestamp
        }
      }, // replacement
      {}, // options
      function(err, object) {
        if (err){
            console.warn(err.message);  // returns error if no matching object found
            console.warn("error"); 
        }
        else{
          if (object){
          }
          else{
          }
        }
      }
    )
  })
}

function storageData(data){
  var url = connectDatabase();
  console.log("URL: " + url);
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    } 
    db.collection('noizchain').findOne(
      {
        "email": data.email
      },
      function(err, object) {
        if (err){
          console.log(err);
          console.log("error");
        }
        else{
          if (object){
            db.collection('noizchain').update(
              {"email":data.email},
              {
                $set:{
                  "name":data.name,
                  "email":data.email,
                  "gender":data.gender,
                  "country":data.country,
                  "age":data.age,
                }
              }
            )
            console.log("update");
          }
          else{
            db.collection('noizchain').insert(data,function(err,record){
            })
            console.log("insert");
          }
        }
      }
    )
  })
}
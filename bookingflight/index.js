process.env.GOOGLE_APPLICATION_CREDENTIALS = 'file.txt'
var express = require('express');
var MongoClient = require('mongodb').MongoClient;

var app = express();
app.set("view engine", "ejs");
app.set("views","./views");
var server = require("http").Server(app);
var io = require('socket.io')(server);
server.listen(process.env.PORT || 9003);

// You can find your project ID in your Dialogflow agent settings
const projectId = 'noizchain-connor-bookingflight';
 
// Instantiate a DialogFlow client.
const dialogflow = require('dialogflow');
const sessionClient = new dialogflow.SessionsClient();

// Define session path

io.on("connection", function(socket){
  const sesId = socket.id;
  const sessionPath = sessionClient.sessionPath(projectId, sesId);
  var language = "en-US"; 
 
  socket.on("init_data",function(data){
    data.session_id = sesId;
    initMess(data,sesId);
  })
  socket.on("clientMess_2FA",function(data){
    clientMess(data,sesId);
  })
  socket.on("botMess_2FA",function(data){
    // var newData = configData(data);
    botMess(data,sesId);
  })
  socket.on("chatbot",function(data){
    clientMess(data,sesId);
    
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: data,
                languageCode: language,//en-US
            },
        },
    };

    sessionClient.detectIntent(request).then(responses => {
      const result = responses[0].queryResult;
      // console.log(result.parameters.fields);
      // console.log(result.intent.displayName);
      
      if (result.fulfillmentText != ""){ //normal response and webhook response
        // console.log("1");
        var str = result.fulfillmentText;
        if (str.indexOf("phone number") > 0){
          // console.log("phone number");
          socket.emit("setPhoneNumber","true");
        }
        var data_type = configData(result.fulfillmentText);
        socket.emit("chatbot",data_type);
        botMess(data_type,sesId);
      }
      else if (result.fulfillmentMessages[0].payload){ //custom response
        // console.log("2");
        socket.emit("chatbot",result.fulfillmentMessages[0].payload.fields);
        botMess(result.fulfillmentMessages[0].payload.fields,sesId);
      } 
      else if (result.fulfillmentMessages[0].text){ //question's parameter response
        // console.log("3");
        var data_type = configData(result.fulfillmentMessages[0].text.text[0]);
        socket.emit("chatbot",data_type);
        botMess(data_type,sesId);
      }
    })
    .catch(err => {
      console.error('ERRORerr: ' + err);
    });
  })
})

function configData(data){
  var data_type = {
    'response':{
      'listValue':{
        'values':[
          {
            'stringValue': data,
            'kind': 'stringValue'
          }
        ]
      },
      'kind': 'listValue'
    },
    'answer':{
      'listValue':{
        'values':[],
      },
      'kind': 'listValue'
    }
  }
  return data_type;
}
function connectDatabase(){
  var dbuser = 'noizchain_connor_1';
  var dbpassword = 'a123456789';
  var database = 'noizchain_connor_project_1';

  var url = 'mongodb://' + dbuser + ":" + dbpassword + "@ds121871.mlab.com:21871/" + database;

  return url;
}

function initMess(data,sessionId){
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    } 
    db.collection('booking_flight').findOne(
      {
        "session_id": sessionId
      },
      function(err, object) {
        if (err){
          console.log(err);
          console.log("error");
        }
        else{
          if (object){
          }
          else{
            db.collection('booking_flight').insert(data,function(err,record){
            })
          }
        }
      }
    )
  })
}
function botMess(data,sessionId){
  var time = getTime();
  var timestamp = new Object();
  timestamp[time] =  {
    'who' : "bot",
    'response' : data.response.listValue.values,
    'answer' : data.answer.listValue.values
  };
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    }
    db.collection('booking_flight').findAndModify(
      {
        "session_id": sessionId
      }, // query
      [['_id','asc']],  // sort order
      {
        $push:{
          timestamp:timestamp
        }
      }, // replacement
      {}, // options
      function(err, object) {
          if (err){
              console.warn(err.message); 
              console.warn("error"); 
          }
          else{
              if (object){
              }
              else{
              }
          }
      }
    )
  })
}

function clientMess(data,sessionId){
  var time = getTime();
  var timestamp = new Object();
  timestamp[time] =  {
    'who' : "client",
    'response' : data
  };
  var url = connectDatabase();
  MongoClient.connect(url, function(err, db) {
    if (err != null) {
        throw err;
    }
    db.collection('booking_flight').findAndModify(
      {
        "session_id": sessionId
      }, // query
      [['_id','asc']],  // sort order
      {
        $push:{
          timestamp:timestamp
        }
      }, // replacement
      {}, // options
      function(err, object) {
        if (err){
            console.warn(err.message);  // returns error if no matching object found
            console.warn("error"); 
        }
        else{
          if (object){
          }
          else{
          }
        }
      }
    )
  })
}
function getTime(){
  var d = new Date();
  var day = d.getDate();
  var month = d.getMonth() + 1;
  var year = d.getFullYear();
  var hour = d.getHours();
  var minute = d.getMinutes();
  var second = d.getSeconds();

  var time = hour + ":" + minute + ":" + second + " | " + month + "/" + day + "/" + year;
  return time;
}
